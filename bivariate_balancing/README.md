### README for bivariate balancing

Reads in a CoST and makes all faces convex. Outputs new CoST as OFF.

###### To run:

In build/src/OpenMesh/Apps/bivariate_balancing:
```
./bivariate_balancing infile.off outfile.off
```
where `infile.off` is a CoST.

