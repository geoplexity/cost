/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <bivariate_balancing.h>
// ----------------------------------------------------------------------------
// reads in a mesh from argv[1] and outputs mesh to argv[2]

int CGAL_fix( char* filename)
{
  std::ifstream input( filename);
  if (!input)
  {
   std::cerr << "Cannot open file " << std::endl;
   return 1;
  }
  std::vector<K::Point_3> points;
  std::vector< std::vector<std::size_t> > polygons;
  if ( !CGAL::read_OFF( input, points, polygons))
  {
   std::cerr << "Error parsing the OFF file " << std::endl;
   return 1;
  }
  CGAL::Polygon_mesh_processing::orient_polygon_soup( points, polygons);
  Polyhedron mesh;
  CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh( points, polygons, mesh);
  if (CGAL::is_closed(mesh) && (!CGAL::Polygon_mesh_processing::is_outward_oriented( mesh)))
   CGAL::Polygon_mesh_processing::reverse_face_orientations(mesh);
  std::ofstream out( filename);
  out << mesh;
  out.close();
  return 0;
}




int main( int argc, char **argv)
{
  MeshUtil mesh_util;
  // check command line options
  if (argc != 3)
  {
    std::cerr << "Usage:  " << argv[0] << " #iterations infile outfile\n";
    return 1;
  }
  if ( ! OpenMesh::IO::read_mesh( mesh_util.mesh, argv[1]) )
  {
    std::cerr << "Error: Cannot read mesh from " << argv[1] << std::endl;
    return 1;
  }

  mesh_util.fill_in_faces();

  if ( ! OpenMesh::IO::write_mesh( mesh_util.mesh, argv[2]) )
  {
    std::cerr << "Error: cannot write mesh to " << argv[2] << std::endl;
    return 1;
  }

  CGAL_fix( argv[2]);

  if ( ! OpenMesh::IO::read_mesh( mesh_util.mesh, argv[2]) )
  {
    std::cerr << "Error: Cannot read mesh from " << argv[2] << std::endl;
    return 1;
  }

  mesh_util.convexify();

  mesh_util.add_mass_redux();

  if ( ! OpenMesh::IO::write_mesh( mesh_util.mesh, argv[2]) )
  {
    std::cerr << "Error: cannot write mesh to " << argv[2] << std::endl;
    return 1;
  }

  return 0;
}
