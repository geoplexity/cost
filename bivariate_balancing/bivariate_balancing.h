/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
// bivariate_balancing.h

#ifndef _BIVARIATE_BALANCING_H_
#define _BIVARIATE_BALANCING_H_
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath> // for cos, sin
#include <utility>
#include <unordered_map>
// -------------------- Boost
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/math/constants/constants.hpp> // for pi
// -------------------- OpenMesh
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
// -------------------- CGAL
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/OFF_reader.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>
// ----------------------------------------------------------------------------
typedef boost::numeric::ublas::matrix<double> matrix;
typedef boost::numeric::ublas::vector<double> vector;
// ----------------------------------------------------------------------------
struct MyTraits : public OpenMesh::DefaultTraits
{
  VertexAttributes(OpenMesh::Attributes::Status);
  FaceAttributes(OpenMesh::Attributes::Status);
  EdgeAttributes(OpenMesh::Attributes::Status);
};
typedef OpenMesh::PolyMesh_ArrayKernelT<MyTraits>  MyMesh;
// typedef OpenMesh::VectorT<double, 3> Vec3d;
// -------------------- CGAL
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K> Polyhedron;
// ----------------------------------------------------------------------------

class MeshUtil
{
public:
  MeshUtil() {};
  ~MeshUtil() {};

  MyMesh mesh;

  void convexify();
  void fill_in_faces();
  void add_mass_redux();

private:

  bool is_convex( MyMesh::HalfedgeHandle heh);
  bool is_180_deg( MyMesh::HalfedgeHandle heh);
  void shift_vertex( MyMesh::VertexHandle vh, MyMesh::FaceHandle fh_1, MyMesh::FaceHandle fh_2);

  bool equalish( double a, double b)
  {
    return fabs( a - b) < 0.1;
  }
};

// =============================================================================

// ---------- is_convex()
// takes as input the halfedge pointing to the vertex

// TODO fix to work with clockwise instead of counterclockwise
bool
MeshUtil::is_convex( MyMesh::HalfedgeHandle heh)
{
  std::cout << "is_convex()" << mesh.point( mesh.to_vertex_handle( heh));

  MyMesh::Point p1 = mesh.point( mesh.from_vertex_handle( heh));
  MyMesh::Point p2 = mesh.point( mesh.to_vertex_handle( heh));
  MyMesh::Point p3 = mesh.point( mesh.to_vertex_handle( mesh.next_halfedge_handle( heh)));

  double ccw = ( ( p2[0] - p1[0]) * ( p3[1] - p1[1]))
    - ( ( p2[1] - p1[1]) * ( p3[0] - p1[0]));

  if ( ccw < 0) std::cout << " convex" << std::endl;
  else if ( ccw == 0) std::cout << " 180 deg" << std::endl;
  else std::cout << " non-convex" << std::endl;
  return ( ccw < 0); // TODO should be <
}

// ---------- is_180_deg()
// takes as input the halfedge pointing to the vertex

// TODO fix to work with clockwise instead of counterclockwise
bool
MeshUtil::is_180_deg( MyMesh::HalfedgeHandle heh)
{
  std::cout << "is_180_deg()" << mesh.point( mesh.to_vertex_handle( heh));

  MyMesh::Point p1 = mesh.point( mesh.from_vertex_handle( heh));
  MyMesh::Point p2 = mesh.point( mesh.to_vertex_handle( heh));
  MyMesh::Point p3 = mesh.point( mesh.to_vertex_handle( mesh.next_halfedge_handle( heh)));

  double ccw = ( ( p2[0] - p1[0]) * ( p3[1] - p1[1]))
    - ( ( p2[1] - p1[1]) * ( p3[0] - p1[0]));

  if ( ccw < 0) std::cout << " convex" << std::endl;
  else if ( ccw == 0) std::cout << " 180 deg" << std::endl;
  else std::cout << " non-convex" << std::endl;
  return ( ccw == 0);
}


// --------- shift_vertex()
// moves vertex between two corner sharing triangles to the middle, used in convexify

void
MeshUtil::shift_vertex( MyMesh::VertexHandle vh, MyMesh::FaceHandle fh_1, MyMesh::FaceHandle fh_2)
{
  std::cout << "shift_vertex()" << std::endl;
  MyMesh::HalfedgeHandle heh_1 = mesh.halfedge_handle( fh_1);
  MyMesh::HalfedgeHandle heh_2 = mesh.halfedge_handle( fh_2);

  while ( mesh.from_vertex_handle( heh_1) == vh || mesh.to_vertex_handle( heh_1) == vh)
  {
    heh_1 = mesh.next_halfedge_handle( heh_1);
  }
  while ( mesh.from_vertex_handle( heh_2) == vh || mesh.to_vertex_handle( heh_2) == vh)
  {
    heh_2 = mesh.next_halfedge_handle( heh_2);
  }

  MyMesh::Point point = mesh.point( vh);
  point[0] = ( mesh.point( mesh.to_vertex_handle( heh_1))[0]
    + mesh.point( mesh.to_vertex_handle( heh_2))[0]) / 2.0;
  point[1] = ( mesh.point( mesh.to_vertex_handle( heh_1))[1]
    + mesh.point( mesh.to_vertex_handle( heh_2))[1]) / 2.0;
  mesh.set_point( vh, point);

}

// =============================================================================

void
MeshUtil::convexify()
{
  MyMesh::FaceIter f_it, f_end( mesh.faces_end());
  for ( f_it = mesh.faces_begin(); f_it != f_end; ++f_it)
  {
    MyMesh::VertexHandle vh_begin = mesh.from_vertex_handle( mesh.halfedge_handle( *f_it));
    MyMesh::HalfedgeHandle heh = mesh.halfedge_handle( *f_it);

    do
    {
      if ( is_180_deg( heh))
      {
        shift_vertex( mesh.to_vertex_handle( heh), mesh.face_handle( mesh.opposite_halfedge_handle( heh)),
          mesh.face_handle( mesh.opposite_halfedge_handle( mesh.next_halfedge_handle( heh))));
      }
      heh = mesh.next_halfedge_handle( heh);
    }
    while ( mesh.from_vertex_handle( heh) != vh_begin);
  }

  MyMesh::HalfedgeIter he_it, he_end(mesh.halfedges_end());
  for ( he_it = mesh.halfedges_begin(); he_it != he_end; ++he_it)
  {
    if ( !mesh.is_boundary( *he_it) && !is_convex( *he_it))
    {
      if ( !mesh.is_boundary( mesh.opposite_halfedge_handle( *he_it))
        && !mesh.is_boundary( mesh.opposite_halfedge_handle( mesh.next_halfedge_handle( *he_it))))
      {
        shift_vertex( mesh.to_vertex_handle( *he_it), mesh.face_handle( mesh.opposite_halfedge_handle( *he_it)),
          mesh.face_handle( mesh.opposite_halfedge_handle( mesh.next_halfedge_handle( *he_it))));
      }
    }
  }

}

void
MeshUtil::fill_in_faces()
{
  MyMesh::HalfedgeIter he_it, he_end(mesh.halfedges_end());
  for ( he_it = mesh.halfedges_begin(); he_it != he_end; ++he_it)
  {
    if ( mesh.is_boundary( *he_it))
    {
      MyMesh::VertexHandle vh_begin = mesh.from_vertex_handle( *he_it);
      MyMesh::HalfedgeHandle heh = *he_it;

      bool valid_face = true;

      std::vector<MyMesh::VertexHandle> v_handles;
      size_t edge_count = 0;
      do
      {
        if ( edge_count > 40)
        {
          valid_face = false;
          break;
        }
        v_handles.push_back( mesh.from_vertex_handle( heh));
        heh = mesh.next_halfedge_handle( heh);
        ++edge_count;
      }
      while ( mesh.from_vertex_handle( heh) != vh_begin);

      if ( valid_face) mesh.add_face( v_handles);
    }
  }
}

void
MeshUtil::add_mass_redux()
{
  std::vector<MyMesh::FaceHandle> del_handles;
  MyMesh::FaceIter f_it, f_end( mesh.faces_end());
  for ( f_it = mesh.faces_sbegin(); f_it != f_end; f_it++)
  {
      MyMesh::FaceVertexIter fv_it;
      size_t vcount = 0;
      for (fv_it=mesh.fv_iter( *f_it ); fv_it.is_valid(); ++fv_it)
      {
        vcount++;
      }
      if ( vcount > 3)
      {
        del_handles.push_back( *f_it);
      }
      else if ( vcount == 3)
      {
        MyMesh::FaceFaceIter ff_it;
        size_t adj_tri_count = 0;
        for (ff_it=mesh.ff_iter( *f_it ); ff_it.is_valid(); ++ff_it)
        {
          MyMesh::FaceVertexIter ffv_it;
          size_t ffvcount = 0;
          for (ffv_it=mesh.fv_iter( *ff_it ); ffv_it.is_valid(); ++ffv_it)
          {
            ffvcount++;
          }
          if ( ffvcount == 3)
          {
            adj_tri_count++;
          }
        }
        if ( adj_tri_count == 3)
        {
          del_handles.push_back( *f_it);
        }
      }
  }

  for ( size_t i = 0; i < del_handles.size(); ++i)
  {
    mesh.delete_face( del_handles[i], true);
  }

  mesh.garbage_collection();

}

#endif
