include (ACGCommon)
include_directories (
  ../../..
  ${CMAKE_CURRENT_SOURCE_DIR}
)
set (targetName bivariate_balancing)
# collect all header and source files
acg_append_files (headers "*.h" .)
acg_append_files (sources "*.cpp" .)
acg_add_executable (${targetName} ${headers} ${sources})
target_link_libraries (${targetName}
  OpenMeshCore
  OpenMeshTools
  CGAL
  gmp
)
