### README for trivariate Cristobalite

###### To run:

In build/src/OpenMesh/Apps/trivariate_Cristobalite:
```
./trivariate_Cristobalite infile.off outfile.off number_of_layers
```
where `infile.off` is an OFF file that describes a CoST, and `number_of_layers` is
the number of stacked layers of the CoST to add to create the cristobalite structure.
For example, `infile.off outfile.off 5` will output a cristobalite to `outfile.off`
that is 5 layers high.
