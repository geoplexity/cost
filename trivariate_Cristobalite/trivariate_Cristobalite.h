/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
// trivariate_Cristobalite.h

#ifndef _TRIVARIATE_CRISTOBALITE_H_
#define _TRIVARIATE_CRISTOBALITE_H_
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath> // for cos, sin
#include <utility>
#include <unordered_map>
// -------------------- Boost
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/math/constants/constants.hpp> // for pi
// -------------------- OpenMesh
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
// -------------------- CGAL
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/OFF_reader.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>
// ----------------------------------------------------------------------------
typedef boost::numeric::ublas::matrix<double> matrix;
typedef boost::numeric::ublas::vector<double> vector;
// ----------------------------------------------------------------------------
struct MyTraits : public OpenMesh::DefaultTraits
{
  VertexAttributes(OpenMesh::Attributes::Status);
  FaceAttributes(OpenMesh::Attributes::Status);
  EdgeAttributes(OpenMesh::Attributes::Status);
};
typedef OpenMesh::PolyMesh_ArrayKernelT<MyTraits>  MyMesh;
// typedef OpenMesh::VectorT<double, 3> Vec3d;
// -------------------- CGAL
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K> Polyhedron;
// ----------------------------------------------------------------------------

class MeshUtil
{
public:
  MeshUtil() {};
  ~MeshUtil() {};

  MyMesh mesh;

  void fill_in_faces();
  void add_mass_redux();

  void add_layers( size_t num_layers);
  void connect_layers( size_t num_layers);
  bool is_triangle( MyMesh::FaceHandle fh);
  bool is_z_0( MyMesh::FaceHandle fh);
  bool check_layer( MyMesh::FaceHandle fh, double layer_z_position);
  std::vector<MyMesh::FaceHandle> find_face_handle( MyMesh::VertexHandle v0, MyMesh::VertexHandle v1, MyMesh::VertexHandle v2);

  void irregulate( MyMesh::FaceHandle fh1, MyMesh::FaceHandle fh2);

private:


  bool is_convex( MyMesh::HalfedgeHandle heh);
  bool is_180_deg( MyMesh::HalfedgeHandle heh);

  bool contains( std::vector<MyMesh::FaceHandle> f_handles, MyMesh::FaceHandle fh);
  void add_faces_tetrahedron( std::vector<MyMesh::FaceHandle> f_handles, std::vector<MyMesh::FaceHandle> f_handles_top);
  std::vector<MyMesh::FaceHandle> find_top_pairs( std::vector<MyMesh::FaceHandle> f_handles, int layer_z_position);

  std::vector<double> find_center( MyMesh::FaceHandle fh);

  bool equalish( double a, double b)
  {
    return fabs( a - b) < 0.1;
  }
  bool compare_cent( double a, double b)
  {
    return fabs( a - b) < 0.2;
  }
};

// ---------- is_convex()
// takes as input the halfedge pointing to the vertex

// TODO fix to work with clockwise instead of counterclockwise
bool
MeshUtil::is_convex( MyMesh::HalfedgeHandle heh)
{
  std::cout << "is_convex()" << mesh.point( mesh.to_vertex_handle( heh));

  MyMesh::Point p1 = mesh.point( mesh.from_vertex_handle( heh));
  MyMesh::Point p2 = mesh.point( mesh.to_vertex_handle( heh));
  MyMesh::Point p3 = mesh.point( mesh.to_vertex_handle( mesh.next_halfedge_handle( heh)));

  double ccw = ( ( p2[0] - p1[0]) * ( p3[1] - p1[1]))
    - ( ( p2[1] - p1[1]) * ( p3[0] - p1[0]));

  if ( ccw < 0) std::cout << " convex" << std::endl;
  else if ( ccw == 0) std::cout << " 180 deg" << std::endl;
  else std::cout << " non-convex" << std::endl;
  return ( ccw < 0); // TODO should be <
}

// ---------- is_180_deg()
// takes as input the halfedge pointing to the vertex

// TODO fix to work with clockwise instead of counterclockwise
bool
MeshUtil::is_180_deg( MyMesh::HalfedgeHandle heh)
{
  std::cout << "is_180_deg()" << mesh.point( mesh.to_vertex_handle( heh));

  MyMesh::Point p1 = mesh.point( mesh.from_vertex_handle( heh));
  MyMesh::Point p2 = mesh.point( mesh.to_vertex_handle( heh));
  MyMesh::Point p3 = mesh.point( mesh.to_vertex_handle( mesh.next_halfedge_handle( heh)));

  double ccw = ( ( p2[0] - p1[0]) * ( p3[1] - p1[1]))
    - ( ( p2[1] - p1[1]) * ( p3[0] - p1[0]));

  if ( ccw < 0) std::cout << " convex" << std::endl;
  else if ( ccw == 0) std::cout << " 180 deg" << std::endl;
  else std::cout << " non-convex" << std::endl;
  return ( ccw == 0);
}

void
MeshUtil::fill_in_faces()
{
  MyMesh::HalfedgeIter he_it, he_end(mesh.halfedges_end());
  for ( he_it = mesh.halfedges_begin(); he_it != he_end; ++he_it)
  {
    if ( mesh.is_boundary( *he_it))
    {
      MyMesh::VertexHandle vh_begin = mesh.from_vertex_handle( *he_it);
      MyMesh::HalfedgeHandle heh = *he_it;

      bool valid_face = true;

      std::vector<MyMesh::VertexHandle> v_handles;
      size_t edge_count = 0;
      do
      {
        if ( edge_count > 40)
        {
          valid_face = false;
          break;
        }
        v_handles.push_back( mesh.from_vertex_handle( heh));
        heh = mesh.next_halfedge_handle( heh);
        ++edge_count;
      }
      while ( mesh.from_vertex_handle( heh) != vh_begin);

      if ( valid_face) mesh.add_face( v_handles);
    }
  }
}

void
MeshUtil::add_mass_redux()
{
  std::vector<MyMesh::FaceHandle> del_handles;
  MyMesh::FaceIter f_it, f_end( mesh.faces_end());
  for ( f_it = mesh.faces_sbegin(); f_it != f_end; f_it++)
  {
      MyMesh::FaceVertexIter fv_it;
      size_t vcount = 0;
      for (fv_it=mesh.fv_iter( *f_it ); fv_it.is_valid(); ++fv_it)
      {
        vcount++;
      }
      if ( vcount > 3)
      {
        del_handles.push_back( *f_it);
      }
      else if ( vcount == 3)
      {
        MyMesh::FaceFaceIter ff_it;
        size_t adj_tri_count = 0;
        for (ff_it=mesh.ff_iter( *f_it ); ff_it.is_valid(); ++ff_it)
        {
          MyMesh::FaceVertexIter ffv_it;
          size_t ffvcount = 0;
          for (ffv_it=mesh.fv_iter( *ff_it ); ffv_it.is_valid(); ++ffv_it)
          {
            ffvcount++;
          }
          if ( ffvcount == 3)
          {
            adj_tri_count++;
          }
        }
        if ( adj_tri_count == 3)
        {
          del_handles.push_back( *f_it);
        }
      }
  }

  for ( size_t i = 0; i < del_handles.size(); ++i)
  {
    mesh.delete_face( del_handles[i], true);
  }

  mesh.garbage_collection();

}

void
MeshUtil::add_layers( size_t num_layers)
{
  std::vector< std::vector<MyMesh::VertexHandle> > new_faces;

  for ( size_t layer = 1; layer <= num_layers; layer++)
  {
    int counter = 0;
    for ( MyMesh::FaceIter f_it = mesh.faces_sbegin(); f_it != mesh.faces_end(); ++f_it)
    {
      if ( equalish( mesh.point(mesh.from_vertex_handle(mesh.halfedge_handle(*f_it)))[2], 1.73205*(layer-1)) )
      {
        // std::cout << mesh.point(mesh.from_vertex_handle(mesh.halfedge_handle(*f_it))) << std::endl;
        counter++;
        std::vector<MyMesh::VertexHandle> v_handles;

        for ( MyMesh::FaceHalfedgeIter fh_it = mesh.fh_iter( *f_it ); fh_it.is_valid(); ++fh_it)
        {
            MyMesh::EdgeHandle eh = mesh.edge_handle( *fh_it);

            MyMesh::VertexHandle vh = mesh.new_vertex();

            // set coordinates of new vertex
            MyMesh::Point point = mesh.point( vh);
            point[0] = mesh.point( mesh.from_vertex_handle( *fh_it))[0];
            point[1] = mesh.point( mesh.from_vertex_handle( *fh_it))[1] + (2 / 1.73205);
            point[2] = mesh.point( mesh.from_vertex_handle( *fh_it))[2] + 1.73205;

            bool already_has_point = false;
            MyMesh::VertexIter v_it, v_end( mesh.vertices_end());
            for ( v_it = mesh.vertices_sbegin(); v_it != v_end; ++v_it)
            {
              if ( equalish( mesh.point( *v_it)[0], point[0])
                && equalish( mesh.point( *v_it)[1], point[1])
                && equalish( mesh.point( *v_it)[2], point[2]))
              {
                already_has_point = true;
                v_handles.push_back( *v_it);
                break;
              }
            }

            if ( !already_has_point)
            {
              MyMesh::VertexHandle new_v = mesh.add_vertex( point);
              v_handles.push_back( new_v);
            }

            mesh.delete_vertex( vh);
        }

        mesh.add_face( v_handles);
      }
    }
    std::cout << "counter " << counter << std::endl;
  }

  double max_y = 0;
  double min_y = 100;
  for ( MyMesh::VertexIter v_it = mesh.vertices_sbegin(); v_it != mesh.vertices_end(); ++v_it)
  {
    if ( equalish( mesh.point( *v_it)[2], 0) && mesh.point( *v_it)[1] > max_y)
    {
      max_y = mesh.point( *v_it)[1];
    }
    else if ( equalish( mesh.point( *v_it)[2], 1.73205*num_layers) && mesh.point( *v_it)[1] < min_y)
    {
      min_y = mesh.point( *v_it)[1];
    }
  }
  std::cout << "min_y " << min_y << " max_y " << max_y << std::endl;

  std::vector<MyMesh::FaceHandle> del_handles;
  for ( MyMesh::FaceIter f_it = mesh.faces_sbegin(); f_it != mesh.faces_end(); ++f_it)
  {
    std::vector<double> centroid = find_center( *f_it);
    if ( centroid[1] > max_y || centroid[1] < min_y)
    {
      del_handles.push_back( *f_it);
    }
  }

  for ( size_t i = 0; i < del_handles.size(); ++i)
  {
    mesh.delete_face( del_handles[i], true);
  }

  mesh.garbage_collection();

 }

bool
MeshUtil::is_triangle( MyMesh::FaceHandle fh)
{
  size_t v_count = 0;
  for ( MyMesh::FaceVertexIter fv_it = mesh.fv_iter( fh); fv_it.is_valid(); ++fv_it)
  {
    v_count++;
  }
  if ( v_count == 3) return true;
  else return false;
}

bool
MeshUtil::is_z_0( MyMesh::FaceHandle fh)
{
  bool success = true;
  for ( MyMesh::FaceVertexIter fv_it = mesh.fv_iter( fh); fv_it.is_valid(); ++fv_it)
  {
    if ( ! equalish( mesh.point( *fv_it)[2], 0))
    {
      success = false;
      break;
    }
  }
  return success;
}

bool
MeshUtil::check_layer( MyMesh::FaceHandle fh, double layer_z_position)
{
  bool success = true;
  for ( MyMesh::FaceVertexIter fv_it = mesh.fv_iter( fh); fv_it.is_valid(); ++fv_it)
  {
    if ( ! equalish( mesh.point( *fv_it)[2], layer_z_position))
    {
      success = false;
      break;
    }
  }
  return success;
}

// sorry this is terrible
// checks to see if a face (fh) is in f_handles
bool
MeshUtil::contains( std::vector<MyMesh::FaceHandle> f_handles, MyMesh::FaceHandle fh)
{
  bool success = false;
  for ( size_t i = 0; i < f_handles.size(); i++)
  {

    size_t match = 0;
    for ( MyMesh::FaceVertexIter fv_it = mesh.fv_iter( f_handles[i]); fv_it.is_valid(); ++fv_it)
    {
      for ( MyMesh::FaceVertexIter fv_it_fh = mesh.fv_iter( fh); fv_it_fh.is_valid(); ++fv_it_fh)
      {
        if ( equalish( mesh.point( *fv_it)[0], mesh.point( *fv_it_fh)[0])
          && equalish( mesh.point( *fv_it)[1], mesh.point( *fv_it_fh)[1])
          && equalish( mesh.point( *fv_it)[2], mesh.point( *fv_it_fh)[2]))
        {
          match++;
          break;
        }
      }
    }

    if ( match == 3)
    {
      success = true;
    }
  }

  return success;
}

std::vector<double>
MeshUtil::find_center( MyMesh::FaceHandle fh)
{
  std::vector<MyMesh::VertexHandle> vertices_around_face;
  for ( MyMesh::FaceVertexCWIter fv_it = mesh.fv_cwiter( fh); fv_it.is_valid(); ++fv_it)
  {
    vertices_around_face.push_back( *fv_it);
  }

  std::vector<double> centroid;

  centroid.push_back( ( mesh.point( vertices_around_face[0])[0]
    + mesh.point( vertices_around_face[1])[0]
    + mesh.point( vertices_around_face[2])[0]) / 3);
  centroid.push_back( ( mesh.point( vertices_around_face[0])[1]
    + mesh.point( vertices_around_face[1])[1]
    + mesh.point( vertices_around_face[2])[1]) / 3);
  centroid.push_back( ( mesh.point( vertices_around_face[0])[2]
    + mesh.point( vertices_around_face[1])[2]
    + mesh.point( vertices_around_face[2])[2]) / 3);

  return centroid;
}

void
MeshUtil::add_faces_tetrahedron( std::vector<MyMesh::FaceHandle> f_handles, std::vector<MyMesh::FaceHandle> f_handles_top)
{
  for ( size_t k = 0; k < f_handles.size(); k++)
  {
    std::vector<MyMesh::VertexHandle> vertices_around_face;
    for ( MyMesh::FaceVertexCWIter fv_it = mesh.fv_cwiter( f_handles[k]); fv_it.is_valid(); ++fv_it)
    {
      vertices_around_face.push_back( *fv_it);
    }

    std::vector<MyMesh::VertexHandle> vertices_around_face_top;
    for ( MyMesh::FaceVertexCWIter fv_it = mesh.fv_cwiter( f_handles_top[k]); fv_it.is_valid(); ++fv_it)
    {
      vertices_around_face_top.push_back( *fv_it);
    }

    for ( size_t m = 0; m < vertices_around_face.size(); m++)
    {
      std::cout << mesh.point( vertices_around_face[m]) << std::endl;
    }
    for ( size_t m = 0; m < vertices_around_face_top.size(); m++)
    {
      std::cout << mesh.point( vertices_around_face_top[m]) << std::endl;
    }

    MyMesh::VertexHandle vh = mesh.new_vertex();
    // set coordinates of new vertex
    MyMesh::Point centroid = mesh.point( vh);
    centroid[0] = ( mesh.point( vertices_around_face[0])[0]
      + mesh.point( vertices_around_face[1])[0]
      + mesh.point( vertices_around_face[2])[0]) / 3;
    centroid[1] = ( mesh.point( vertices_around_face[0])[1]
      + mesh.point( vertices_around_face[1])[1]
      + mesh.point( vertices_around_face[2])[1]) / 3;
    centroid[2] = mesh.point( vertices_around_face[0])[2] + (1.73205 / 2); // TODO calculate height

    MyMesh::VertexHandle new_v = mesh.add_vertex( centroid);
    MyMesh::VertexHandle v0 = mesh.add_vertex( mesh.point(vertices_around_face[0]));
    MyMesh::VertexHandle v1 = mesh.add_vertex( mesh.point(vertices_around_face[1]));
    MyMesh::VertexHandle v2 = mesh.add_vertex( mesh.point(vertices_around_face[2]));

    MyMesh::VertexHandle new_v_top = mesh.add_vertex( centroid);
    MyMesh::VertexHandle v3 = mesh.add_vertex( mesh.point(vertices_around_face_top[0]));
    MyMesh::VertexHandle v4 = mesh.add_vertex( mesh.point(vertices_around_face_top[1]));
    MyMesh::VertexHandle v5 = mesh.add_vertex( mesh.point(vertices_around_face_top[2]));

    mesh.add_face( new_v, v0, v1);
    mesh.add_face( new_v, v1, v2);
    mesh.add_face( new_v, v2, v0);

    mesh.add_face( v5, v3, new_v_top);
    mesh.add_face( v3, v4, new_v_top);
    mesh.add_face( v4, v5, new_v_top);

    mesh.delete_vertex( vh);

  }
}

std::vector<MyMesh::FaceHandle>
MeshUtil::find_top_pairs( std::vector<MyMesh::FaceHandle> f_handles, int layer_z_position)
{
  std::vector<MyMesh::FaceHandle> f_handles_top;
  MyMesh::FaceIter f_it;
  for ( size_t j = 0; j < f_handles.size(); j++)
  {
    bool found_match = false;
    for ( f_it = mesh.faces_sbegin(); f_it != mesh.faces_end(); ++f_it)
    {
      size_t match = 0;
      if ( check_layer( *f_it, layer_z_position))
      {
        for ( MyMesh::FaceVertexIter fv_it = mesh.fv_iter( f_handles[j]); fv_it.is_valid(); ++fv_it)
        {
          for ( MyMesh::FaceVertexIter fv_it_fh = mesh.fv_iter( *f_it); fv_it_fh.is_valid(); ++fv_it_fh)
          {
            if ( equalish( mesh.point( *fv_it)[0], mesh.point( *fv_it_fh)[0])
              && equalish( mesh.point( *fv_it)[1], mesh.point( *fv_it_fh)[1])
              && equalish( mesh.point( *fv_it_fh)[2], layer_z_position))
            {
              match++;
              break;
            }
          }
        }

        std::cout << "match " << match << std::endl;
        if ( match == 3)
        {
          f_handles_top.push_back( *f_it);
          found_match = true;
          break;
        }
      }
    }
    if ( !found_match)
    {
      throw std::runtime_error( "MeshUtil::connect_layers() missing pair");
    }
  }
  return f_handles_top;
}

void
MeshUtil::connect_layers( size_t num_layers)
{
  std::vector<MyMesh::FaceHandle> f_handles;
  std::vector<MyMesh::FaceHandle> f_handles_top;
  MyMesh::FaceIter f_it;
  MyMesh::FaceIter f_it2;

  for ( f_it = mesh.faces_sbegin(); f_it != mesh.faces_end(); ++f_it)
  {
    if ( is_triangle( *f_it) && check_layer( *f_it, 0))
    {
      std::vector<double> centroid_bottom = find_center( *f_it);
      for ( f_it2 = mesh.faces_sbegin(); f_it2 != mesh.faces_end(); ++f_it2)
      {
        if ( is_triangle( *f_it2) && check_layer( *f_it2, 1.73205))
        {
          std::vector<double> centroid_top = find_center( *f_it2);
          if ( compare_cent( centroid_bottom[0], centroid_top[0])
          && compare_cent( centroid_bottom[1], centroid_top[1]))
          {
            f_handles.push_back( *f_it);
            f_handles_top.push_back( *f_it2);
            break;
          }
        }
      }
    }
  }

  add_faces_tetrahedron( f_handles, f_handles_top);

  // next layers ----------------------------------------------------------------
  for ( size_t layer = 1; layer < num_layers; layer++)
  {
    f_handles.clear();
    f_handles_top.clear();
    for ( f_it = mesh.faces_sbegin(); f_it != mesh.faces_end(); ++f_it)
    {
      if ( is_triangle( *f_it) && check_layer( *f_it, (1.73205*layer)))
      {
        std::vector<double> centroid_bottom = find_center( *f_it);
        for ( f_it2 = mesh.faces_sbegin(); f_it2 != mesh.faces_end(); ++f_it2)
        {
          if ( is_triangle( *f_it2) && check_layer( *f_it2, (1.73205*(layer+1))))
          {
            std::vector<double> centroid_top = find_center( *f_it2);
            if ( compare_cent( centroid_bottom[0], centroid_top[0])
            && compare_cent( centroid_bottom[1], centroid_top[1]))
            {
              f_handles.push_back( *f_it);
              f_handles_top.push_back( *f_it2);
              break;
            }
          }
        }
      }
    }
    add_faces_tetrahedron( f_handles, f_handles_top);
  }

  mesh.garbage_collection();

}

std::vector<MyMesh::FaceHandle>
MeshUtil::find_face_handle( MyMesh::VertexHandle v0, MyMesh::VertexHandle v1, MyMesh::VertexHandle v2)
{
  std::vector<MyMesh::FaceHandle> f_handles;
  for ( MyMesh::FaceIter f_it = mesh.faces_begin(); f_it != mesh.faces_end(); ++f_it)
  {
    for ( MyMesh::FaceVertexIter fv_it = mesh.fv_iter( *f_it); fv_it.is_valid(); ++fv_it)
    {
      if ( equalish( mesh.point( *fv_it)[0], mesh.point( v0)[0])
        && equalish( mesh.point( *fv_it)[1], mesh.point( v0)[1])
        && equalish( mesh.point( *fv_it)[2], mesh.point( v0)[2]))
      {
        for ( MyMesh::FaceVertexIter fv_it1 = mesh.fv_iter( *f_it); fv_it1.is_valid(); ++fv_it1)
        {
          if ( equalish( mesh.point( *fv_it1)[0], mesh.point( v1)[0])
            && equalish( mesh.point( *fv_it1)[1], mesh.point( v1)[1])
            && equalish( mesh.point( *fv_it1)[2], mesh.point( v1)[2]))
          {
            for ( MyMesh::FaceVertexIter fv_it2 = mesh.fv_iter( *f_it); fv_it2.is_valid(); ++fv_it2)
            {
              if ( equalish( mesh.point( *fv_it2)[0], mesh.point( v2)[0])
                && equalish( mesh.point( *fv_it2)[1], mesh.point( v2)[1])
                && equalish( mesh.point( *fv_it2)[2], mesh.point( v2)[2]))
              {
                f_handles.push_back( *f_it);
                break;
              }
            }
            break;
          }
        }
        break;
      }
    }
  }
  return f_handles;
}

void
MeshUtil::irregulate( MyMesh::FaceHandle fh1, MyMesh::FaceHandle fh2)
{
  // pivot is the vertex that the two faces share
  MyMesh::VertexHandle pivot;
  for ( MyMesh::FaceVertexIter fv_it = mesh.fv_iter( fh1); fv_it.is_valid(); ++fv_it)
  {
    bool match = false;
    for ( MyMesh::FaceVertexIter fv_it2 = mesh.fv_iter( fh2); fv_it2.is_valid(); ++fv_it2)
    {
      if ( mesh.point( *fv_it) == mesh.point( *fv_it2))
      {
        pivot = *fv_it;
        match = true;
        break;
      }
    }
    if ( match) break;
  }
  std::cout << "pivot is " << mesh.point( pivot) << std::endl;

  // center 1 is the peak of the tetrahedron whose base is fh1
  MyMesh::VertexHandle center1;
  std::vector<double> c1_vec = find_center( fh1);
  for ( size_t i = 0; i < c1_vec.size(); i++)
  {
    std::cout << "c1_vec[" << i << "] is " << c1_vec[i] << std::endl;
  }
  for ( MyMesh::VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it)
  {
    if ( compare_cent( mesh.point( *v_it)[0], c1_vec[0])
      && compare_cent( mesh.point( *v_it)[1], c1_vec[1])
      && fabs( mesh.point( *v_it)[2] - c1_vec[2]) < 1)
    {
      center1 = *v_it;
      break;
    }
  }
  std::cout << "center1 is " << mesh.point( center1) << std::endl;

  // center 2 is the peak of the tetrahedron whose base is fh2
  MyMesh::VertexHandle center2;
  std::vector<double> c2_vec = find_center( fh2);
  for ( size_t i = 0; i < c2_vec.size(); i++)
  {
    std::cout << "c2_vec[" << i << "] is " << c2_vec[i] << std::endl;
  }
  for ( MyMesh::VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it)
  {
    if ( compare_cent( mesh.point( *v_it)[0], c2_vec[0])
      && compare_cent( mesh.point( *v_it)[1], c2_vec[1])
      && fabs( mesh.point( *v_it)[2] - c2_vec[2]) < 1)
    {
      center2 = *v_it;
      break;
    }
  }
  std::cout << "center2 is " << mesh.point( center2) << std::endl;

  MyMesh::VertexHandle v1, v2, u1, u2;
  std::vector<MyMesh::VertexHandle> fh1_v1v2;
  for ( MyMesh::FaceVertexIter fv_it = mesh.fv_iter( fh1); fv_it.is_valid(); ++fv_it)
  {
    if ( *fv_it != pivot)
    {
      fh1_v1v2.push_back( *fv_it);
    }
  }
  std::vector<MyMesh::VertexHandle> fh2_u1u2;
  for ( MyMesh::FaceVertexIter fv_it = mesh.fv_iter( fh2); fv_it.is_valid(); ++fv_it)
  {
    if ( *fv_it != pivot)
    {
      fh2_u1u2.push_back( *fv_it);
    }
  }

  // Case 1: non-pivot vertices of fh1 have similar y-values
  if ( equalish( mesh.point( fh1_v1v2[0])[1], mesh.point( fh1_v1v2[1])[1]))
  {
    // compare x coordinates with center
    if ( mesh.point( fh1_v1v2[0])[0] > mesh.point( center1)[0])
    {
      v1 = fh1_v1v2[0];
      v2 = fh1_v1v2[1];
    }
    else
    {
      v1 = fh1_v1v2[1];
      v2 = fh1_v1v2[0];
    }
    if ( mesh.point( fh2_u1u2[0])[0] > mesh.point( center2)[0])
    {
      u1 = fh2_u1u2[0];
      u2 = fh2_u1u2[1];
    }
    else
    {
      u1 = fh2_u1u2[1];
      u2 = fh2_u1u2[0];
    }
  }
  // Case 2: non-pivot vertices of fh1 have non-similar y-values
  else
  {
    // compare y coordinates with center
    if ( mesh.point( fh1_v1v2[0])[1] > mesh.point( center1)[1])
    {
      v1 = fh1_v1v2[0];
      v2 = fh1_v1v2[1];
    }
    else
    {
      v1 = fh1_v1v2[1];
      v2 = fh1_v1v2[0];
    }
    if ( mesh.point( fh2_u1u2[0])[1] > mesh.point( center2)[1])
    {
      u1 = fh2_u1u2[0];
      u2 = fh2_u1u2[1];
    }
    else
    {
      u1 = fh2_u1u2[1];
      u2 = fh2_u1u2[0];
    }
  }
  std::cout << "v1 is " << mesh.point( v1) << std::endl;
  std::cout << "v2 is " << mesh.point( v2) << std::endl;
  std::cout << "u1 is " << mesh.point( u1) << std::endl;
  std::cout << "u2 is " << mesh.point( u2) << std::endl;

  std::vector<MyMesh::VertexHandle> v_handles;

  MyMesh::VertexHandle t1 = mesh.add_vertex( mesh.point(pivot));
  MyMesh::VertexHandle t2 = mesh.add_vertex( mesh.point(v1));
  MyMesh::VertexHandle t3 = mesh.add_vertex( mesh.point(u1));
  v_handles.push_back( t1);
  v_handles.push_back( t2);
  v_handles.push_back( t3);
  mesh.add_face( v_handles);
  v_handles.clear();

  t1 = mesh.add_vertex( mesh.point(pivot));
  t2 = mesh.add_vertex( mesh.point(v2));
  t3 = mesh.add_vertex( mesh.point(u2));
  v_handles.push_back( t1);
  v_handles.push_back( t2);
  v_handles.push_back( t3);
  mesh.add_face( v_handles);
  v_handles.clear();

  t1 = mesh.add_vertex( mesh.point(center1));
  t2 = mesh.add_vertex( mesh.point(v1));
  t3 = mesh.add_vertex( mesh.point(u1));
  v_handles.push_back( t1);
  v_handles.push_back( t2);
  v_handles.push_back( t3);
  mesh.add_face( v_handles);
  v_handles.clear();

  t1 = mesh.add_vertex( mesh.point(center2));
  t2 = mesh.add_vertex( mesh.point(v2));
  t3 = mesh.add_vertex( mesh.point(u2));
  v_handles.push_back( t1);
  v_handles.push_back( t2);
  v_handles.push_back( t3);
  mesh.add_face( v_handles);
  v_handles.clear();

  t1 = mesh.add_vertex( mesh.point(center1));
  t2 = mesh.add_vertex( mesh.point(pivot));
  t3 = mesh.add_vertex( mesh.point(u1));
  v_handles.push_back( t1);
  v_handles.push_back( t2);
  v_handles.push_back( t3);
  mesh.add_face( v_handles);
  v_handles.clear();

  t1 = mesh.add_vertex( mesh.point(center2));
  t2 = mesh.add_vertex( mesh.point(pivot));
  t3 = mesh.add_vertex( mesh.point(v2));
  v_handles.push_back( t1);
  v_handles.push_back( t2);
  v_handles.push_back( t3);
  mesh.add_face( v_handles);
  v_handles.clear();

  std::vector<MyMesh::FaceHandle> del_faces;
  std::vector<MyMesh::FaceHandle> f_handles;
  f_handles = find_face_handle( center1, v1, v2);
  for ( size_t i = 0; i < f_handles.size(); i++)
  {
    del_faces.push_back( f_handles[i]);
  }
  f_handles.clear();
  f_handles = find_face_handle( center1, pivot, v2);
  for ( size_t i = 0; i < f_handles.size(); i++)
  {
    del_faces.push_back( f_handles[i]);
  }
  f_handles.clear();
  f_handles = find_face_handle( center2, u1, u2);
  for ( size_t i = 0; i < f_handles.size(); i++)
  {
    del_faces.push_back( f_handles[i]);
  }
  f_handles.clear();
  f_handles = find_face_handle( center2, pivot, u1);
  for ( size_t i = 0; i < f_handles.size(); i++)
  {
    del_faces.push_back( f_handles[i]);
  }
  f_handles.clear();
  del_faces.push_back( fh1);
  del_faces.push_back( fh2);

  std::cout << "del_faces.size() is " << del_faces.size() << std::endl;

  for ( size_t i = 0; i < del_faces.size(); ++i)
  {
    mesh.delete_face( del_faces[i], true);
  }

  mesh.garbage_collection();
}

#endif
