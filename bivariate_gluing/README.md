### README for bivariate gluing


###### To run:

In build/src/OpenMesh/Apps/bivariate_gluing:
```
./bivariate_gluing infile1.off infile2.off outfile.off
```
where `infile1.off` and `infile2.off` are CoSTs.
