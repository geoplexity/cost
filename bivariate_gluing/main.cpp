/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <bivariate_gluing.h>

int main( int argc, char **argv)
{
  MeshUtil mesh_util1;
  MeshUtil mesh_util2;

  // check command line options
  if (argc != 4)
  {
    std::cerr << "Usage:  " << argv[0] << " #iterations infile1 infile2 outfile\n";
    return 1;
  }

  if ( ! OpenMesh::IO::read_mesh( mesh_util1.mesh, argv[1]) )
  {
    std::cerr << "Error: Cannot read mesh from " << argv[1] << std::endl;
    return 1;
  }
  if ( ! OpenMesh::IO::read_mesh( mesh_util2.mesh, argv[2]) )
  {
    std::cerr << "Error: Cannot read mesh from " << argv[2] << std::endl;
    return 1;
  }

  std::vector< std::pair<int, int> > edge_pairs;
  edge_pairs = mesh_util1.find_pairs( mesh_util1.mesh, mesh_util2.mesh);
  mesh_util1.merge_OFF( argv[1], argv[2], edge_pairs, argv[3]);

  if ( ! OpenMesh::IO::write_mesh( mesh_util1.mesh, argv[3]) )
  {
    std::cerr << "Error: cannot write mesh to " << argv[3] << std::endl;
    return 1;
  }

  return 0;
}
