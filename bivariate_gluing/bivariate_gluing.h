/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
// bivariate_gluing.h

#ifndef _BIVARIATE_GLUING_H_
#define _BIVARIATE_GLUING_H_
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath> // for cos, sin
#include <utility>
#include <unordered_map>
// -------------------- Boost
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/math/constants/constants.hpp> // for pi
// -------------------- OpenMesh
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
// -------------------- CGAL
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/OFF_reader.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>
// ----------------------------------------------------------------------------
typedef boost::numeric::ublas::matrix<double> matrix;
typedef boost::numeric::ublas::vector<double> vector;
// ----------------------------------------------------------------------------
struct MyTraits : public OpenMesh::DefaultTraits
{
  VertexAttributes(OpenMesh::Attributes::Status);
  FaceAttributes(OpenMesh::Attributes::Status);
  EdgeAttributes(OpenMesh::Attributes::Status);
};
typedef OpenMesh::PolyMesh_ArrayKernelT<MyTraits>  MyMesh;
// typedef OpenMesh::VectorT<double, 3> Vec3d;
// -------------------- CGAL
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K> Polyhedron;
// ----------------------------------------------------------------------------

class MeshUtil
{
public:
  MeshUtil() {};
  ~MeshUtil() {};

  MyMesh mesh;

  void transform_vertices( MyMesh dont_transform_this_mesh, std::vector< std::pair<int, int> > edge_pairs_index);

  std::vector< std::pair<int, int> > find_pairs( MyMesh mesh1, MyMesh mesh2);

  void merge_OFF( char* off1, char* off2,
    std::vector< std::pair<int, int> > edge_pairs,
    std::string output_file);

  void fill_in_faces();
  void add_mass_redux();

  void stitch_patches( int width, int height, char* patchfile);
  void stitch_different( MyMesh patch1, MyMesh patch2, MyMesh patch3);

private:

  double calc_angle( vector u, vector v);
  matrix calc_rotation( double angle);
  vector make_ublas_vector( double x, double y);
  vector make_ublas_vector( MyMesh::VertexHandle vh, MyMesh mesh);
  vector make_ublas_vector( MyMesh::HalfedgeHandle heh, MyMesh mesh);
  vector calc_translation( vector not_rotated_paired_vertex, vector rotated_paired_vertex);

  std::vector<std::string> readVertices( std::string filename);
  std::vector< std::vector<std::string> >
    readFaces( std::string filename, std::vector<std::string> vertices);
  std::vector<std::string> read_faces( std::string filename);
  std::unordered_map<std::string, std::string>
    map_vertices( std::vector<std::string> vertices1,
    std::vector<std::string> vertices2, std::vector< std::pair<std::string,
    std::string> > pairs);
  std::vector<std::string>
    merge_vertices( std::unordered_map<std::string, std::string> m, int n);
  std::vector< std::vector<std::string> >
    merge_faces( std::unordered_map<std::string, std::string> m,
    std::vector< std::vector<std::string> > faces1,
    std::vector< std::vector<std::string> > faces2);

  void paste_horizontal( int width, double patch_w, std::string patchfile, std::string stripfile);
  void paste_different_horizontal( double patch_w, MyMesh patchL, MyMesh patchR, std::string stripfile);

  bool equalish( double a, double b)
  {
    return fabs( a - b) < 0.1;
  }
  bool compare_vertices( MyMesh::VertexHandle v1, MyMesh mesh1, MyMesh::VertexHandle v2, MyMesh mesh2)
  {
    if ( equalish( mesh1.point( v1)[0], mesh2.point( v2)[0])
      && equalish( mesh1.point( v1)[1], mesh2.point( v2)[1])
      && equalish( mesh1.point( v1)[2], mesh2.point( v2)[2]))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
};

// =============================================================================
// ========== private functions ================================================

// outputs angle between two vectors
// TODO why won't it return negative angles?
double
MeshUtil::calc_angle( vector u, vector v)
{
  return acos( inner_prod( u, v)
    / ( norm_2( u) * norm_2( v)));
}

// outputs rotation matrix for given angle (in rads)
matrix
MeshUtil::calc_rotation( double angle)
{
  matrix r ( 2, 2);
  r( 0, 0) = cos( angle);
  r( 0, 1) = sin( angle) * -1;
  r( 1, 0) = sin( angle);
  r( 1, 1) = cos( angle);
  return r;
}

// like std::make_pair
vector
MeshUtil::make_ublas_vector( double x, double y)
{
  vector temp( 2);
  temp( 0) = x;
  temp( 1) = y;
  return temp;
}

vector
MeshUtil::make_ublas_vector( MyMesh::VertexHandle vh, MyMesh mesh)
{
  vector v( 2);
  v( 0) = mesh.point( vh)[0];
  v( 1) = mesh.point( vh)[1];
  return v;
}

vector
MeshUtil::make_ublas_vector( MyMesh::HalfedgeHandle heh, MyMesh mesh)
{
  vector v( 2);
  v( 0) = mesh.point( mesh.to_vertex_handle( heh))[0]
          - mesh.point( mesh.from_vertex_handle( heh))[0]; // end - begin (x);
  v( 1) = mesh.point( mesh.to_vertex_handle( heh))[1]
          - mesh.point( mesh.from_vertex_handle( heh))[1]; // end - begin (y);
  return v;
}

// calculates translation vector to be added to q to get p
vector
MeshUtil::calc_translation(
      vector not_rotated_paired_vertex,
      vector rotated_paired_vertex)
{
  vector t( 2);
  t( 0) = not_rotated_paired_vertex( 0) - rotated_paired_vertex( 0);
  t( 1) = not_rotated_paired_vertex( 1) - rotated_paired_vertex( 1);
  return t;
}

// ======= operations for merge_OFF ============================================

std::vector<std::string>
MeshUtil::readVertices( std::string filename)
{
  std::ifstream file(filename);

  // ignore first line which is just "OFF"
  std::string dummy;
  std::getline( file, dummy);

  int numV;
  file >> numV;

  // to ignore the rest of the V F 0 line
  std::getline( file, dummy);

  std::vector<std::string> vertices;

  for ( int i = 0; i < numV; i++)
  {
    std::string vertex;
    std::getline( file, vertex);
    vertices.push_back( vertex);
  }

  return vertices;
}

std::vector< std::vector<std::string> >
MeshUtil::readFaces( std::string filename, std::vector<std::string> vertices)
{
  std::ifstream file(filename);

  //ignore first line which is just "OFF"
  std::string dummy;
  std::getline(file, dummy);

  int numV, numF;
  file >> numV >> numF;

  for (int i = 0; i <= numV; i++)
    std::getline(file, dummy);

  std::vector< std::vector<std::string> > faces;

  std::string line;

  while( std::getline( file, line))
  {     // '\n' is the default delimiter
    std::istringstream iss( line);

    std::string token;
    std::vector<std::string> face;
    int count  = 0;
    while( std::getline( iss, token, ' '))
    {
      if ( token.compare( "") != 0 && count > 0) // ignores first number
        face.push_back( token);
      count++;
    }

    faces.push_back( face);
  }

  for ( size_t i = 0; i < faces.size(); i++)
  {
    for ( size_t j = 0; j < faces[i].size(); j++)
    {
      int facesij = std::stoi( faces[i][j]);
      faces[i][j] = vertices[facesij];
    }
  }

  return faces;
}

std::vector<std::string>
MeshUtil::read_faces( std::string filename)
{
  std::ifstream file(filename);

  //ignore first line which is just "OFF"
  std::string dummy;
  std::getline(file, dummy);

  int numV, numF;
  file >> numV >> numF;

  for (int i = 0; i <= numV; i++)
    std::getline(file, dummy);

  std::vector<std::string> faces;
  std::string line;

  while( std::getline( file, line))
  {
    faces.push_back( line);
  }
  return faces;
}


std::unordered_map<std::string, std::string>
MeshUtil::map_vertices( std::vector<std::string> vertices1,
              std::vector<std::string> vertices2,
              std::vector< std::pair<std::string, std::string> > pairs)
{
  std::unordered_map<std::string, std::string> m;

  // mapping vertices from vertices2 to merged vertices v1 through vn
  for ( size_t i = 0; i < vertices2.size(); i++)
  {
    std::ostringstream oss;
    oss << i;
    std::string vi = oss.str();
    oss.str("");
    oss << "DCEL2::" << vertices2[i];
    m[oss.str()] = vi;
  }

  // for each vertex in vertices1, looks for match in pairs, if match then maps
  // to vertex specified in pair, if no match maps to new vertex in m
  int match_count = 0;
  for ( size_t i = 0; i < vertices1.size(); i++)
  {
    bool match = false;
    for ( size_t j = 0; j < pairs.size(); j++)
    {
      if ( pairs[j].first.compare( vertices1[i]) == 0)
      {
        match = true;
        std::ostringstream oss;
        oss << "DCEL1::" << vertices1[i];
        std::string vi = oss.str();
        oss.str("");
        oss << "DCEL2::" << pairs[j].second;
        m[vi] = m[oss.str()];
        ++match_count;
      }
    }
    if ( !match)
    {
      std::ostringstream oss;
      oss << "DCEL1::" << vertices1[i];
      std::string vi = oss.str();
      oss.str("");
      oss << m.size() - match_count;
      m[vi] = oss.str();
    }
  }

  return m;
}

std::vector<std::string>
MeshUtil::merge_vertices( std::unordered_map<std::string, std::string> m, int n)
{
  std::vector<std::string> merged_vertices;

  for ( int i = 0; i < n; i++)
  {
    std::ostringstream oss;
    oss << i;
    std::string iStr = oss.str();
    for ( std::pair<std::string, std::string> element : m)
    {
      if ( element.second.compare( iStr) == 0)
      {
        merged_vertices.push_back( element.first.substr( 7, element.first.size())); // TODO make this less bad
        break;
      }
    }
  }

  return merged_vertices;
}

std::vector< std::vector<std::string> >
MeshUtil::merge_faces( std::unordered_map<std::string, std::string> m,
             std::vector< std::vector<std::string> > faces1,
             std::vector< std::vector<std::string> > faces2)
{
  std::vector< std::vector<std::string> > merged_faces;

  // fill merged_faces with faces from DCEL1, no need to access mapped values of
  // vertices because DCEL1::vi maps to vi
  for ( size_t i = 0; i < faces1.size(); i++)
  {
    std::vector<std::string> face;
    for ( size_t j = 0; j < faces1[i].size(); j++)
    {
      std::ostringstream oss;
      oss << "DCEL1::" << faces1[i][j];
      face.push_back( m[oss.str()]);
    }
    merged_faces.push_back( face);
  }


  // create faces from DCEL2, except using m["DCEL2::vi"] instead of vi, then
  // pushing face to merged_faces
  for ( size_t i = 0; i < faces2.size(); i++)
  {
    std::vector<std::string> face;
    for ( size_t j = 0; j < faces2[i].size(); j++)
    {
      std::ostringstream oss;
      oss << "DCEL2::" << faces2[i][j];
      face.push_back( m[oss.str()]);
    }
    merged_faces.push_back( face);
  }

  return merged_faces;
}

// =============================================================================
//================= operations =================================================

// ---------- transform_vertices()

void
MeshUtil::transform_vertices(
      MyMesh dont_transform_this_mesh,
      std::vector< std::pair<int, int> > edge_pairs_index)
{
  std::cout << "transform_vertices() start" << std::endl;

  MyMesh::HalfedgeHandle heh_dont_transform_this_edge;
  MyMesh::HalfedgeHandle heh_transform_this_edge;

  // outgoing halfedge of first paired vertex from stationary lattice
  heh_dont_transform_this_edge = dont_transform_this_mesh.halfedge_handle( dont_transform_this_mesh.vertex_handle( edge_pairs_index[0].first));

  // outgoing halfedge of first paired vertex from the lattice that we're transforming
  heh_transform_this_edge = mesh.opposite_halfedge_handle( mesh.prev_halfedge_handle( mesh.halfedge_handle( mesh.vertex_handle( edge_pairs_index[0].second))));

  std::cout << "heh_dont_transform_this_edge from " << dont_transform_this_mesh.point( dont_transform_this_mesh.from_vertex_handle( heh_dont_transform_this_edge));
  std::cout << " to " << dont_transform_this_mesh.point( dont_transform_this_mesh.to_vertex_handle( heh_dont_transform_this_edge)) << std::endl;

  std::cout << "heh_transform_this_edge from " << mesh.point( mesh.from_vertex_handle( heh_transform_this_edge));
  std::cout << " to " << mesh.point( mesh.to_vertex_handle( heh_transform_this_edge)) << std::endl;

  vector dont_transform_this_edge = make_ublas_vector( heh_dont_transform_this_edge, dont_transform_this_mesh);
  vector transform_this_edge = make_ublas_vector( heh_transform_this_edge, mesh);

  std::cout << "dont_transform_this_edge is: " << dont_transform_this_edge << std::endl;
  std::cout << "transform_this_edge is: " << transform_this_edge << std::endl;


  // calculate rotation matrix R
  double angle = calc_angle( transform_this_edge, dont_transform_this_edge);
    /*+ boost::math::constants::pi<double>()*/ // added pi here when it kept rotating in opposite of desired direction, seems to work now
  std::cout << "angle is " << angle << std::endl;
  matrix R = calc_rotation( angle);
  std::cout << "R is " << R << std::endl;

  vector rotated_transform_this_edge = prod( R, transform_this_edge);

  if ( rotated_transform_this_edge( 0) != dont_transform_this_edge( 0)
    || rotated_transform_this_edge( 1) != dont_transform_this_edge( 1))
  {
    angle = angle * -1;
    std::cout << "angle is " << angle << std::endl;
    R = calc_rotation( angle);
    std::cout << "R is " << R << std::endl;
  }

  transform_this_edge = prod( R, transform_this_edge);
  std::cout << "transform_this_edge after R is: " << transform_this_edge << std::endl;

  // make ublas vectors from all the vertices in transform_this_mesh
  std::vector<vector> vertex_vectors;
  MyMesh::VertexIter v_it, v_end(mesh.vertices_end());
  for ( v_it = mesh.vertices_begin(); v_it != v_end; ++v_it)
  {
    vertex_vectors.push_back( make_ublas_vector( *v_it, mesh));
  }

  // multiply all vertex vectors by R
  for ( size_t i = 0; i < vertex_vectors.size(); i++)
  {
    vertex_vectors[i] = prod( R, vertex_vectors[i]);
    std::cout << "rotated " << vertex_vectors[i] << std::endl;
  }

  // calculate translation vector t
  vector rotated_paired_vertex = make_ublas_vector( mesh.from_vertex_handle( heh_transform_this_edge), mesh);
  rotated_paired_vertex = prod( R, rotated_paired_vertex);

  vector not_rotated_paired_vertex = make_ublas_vector( dont_transform_this_mesh.from_vertex_handle( heh_dont_transform_this_edge), dont_transform_this_mesh); //TODO repeated code

  vector t = calc_translation( not_rotated_paired_vertex, rotated_paired_vertex);
  std::cout << "t is: " << t << std::endl;

  // add t to all vertex vectors
  for ( size_t i = 0; i < vertex_vectors.size(); i++)
  {
    vertex_vectors[i] = vertex_vectors[i] + t;
    std::cout << "translated " << vertex_vectors[i] << std::endl;
  }

  for ( size_t i = 0; i < vertex_vectors.size(); ++i)
  {
    MyMesh::Point point = mesh.point( mesh.vertex_handle( i));
    point[0] = vertex_vectors[i]( 0);
    point[1] = vertex_vectors[i]( 1);
    mesh.set_point( mesh.vertex_handle( i), point);
  }

}

// ---------- merge_OFF()

void
MeshUtil::merge_OFF(  char* off1, char* off2,
  std::vector< std::pair<int, int> > edge_pairs,
  std::string output_file)
{
  std::vector<std::string> vertices1 = readVertices( off1);
  std::vector<std::string> vertices2 = readVertices( off2);
  std::vector< std::vector<std::string> > faces1 = readFaces( off1, vertices1);
  std::vector< std::vector<std::string> > faces2 = readFaces( off2, vertices2);

  std::vector< std::pair<std::string, std::string> > pairs;
  for ( size_t i = 0; i < edge_pairs.size(); i++)
  {
    pairs.push_back( std::make_pair( vertices1[ edge_pairs[i].first ],
      vertices2[ edge_pairs[i].second ]));
    std::cout << vertices1[ edge_pairs[i].first ] << " paired with "
      << vertices2[ edge_pairs[i].second ] << std::endl;
  }

  std::unordered_map<std::string, std::string> m = map_vertices( vertices1, vertices2, pairs);

  int n = vertices1.size() + vertices2.size() - pairs.size();
  std::vector<std::string> merged_vertices = merge_vertices( m, n);

  std::vector< std::vector<std::string> > merged_faces = merge_faces( m, faces1, faces2);

  std::ofstream file( output_file);

  file << "OFF\n";
  file << merged_vertices.size() << ' ' << merged_faces.size() << " 0\n";

  for ( size_t i = 0; i < merged_vertices.size(); i++)
    file << merged_vertices[i] << '\n';

  for ( size_t i = 0; i < merged_faces.size(); i++)
  {
    file << merged_faces[i].size() << ' ';
    for ( size_t j = 0; j < merged_faces[i].size(); j++)
    {
      file << merged_faces[i][j] << ' ';
    }
    file << '\n';
  }

}

// ---------- find_pairs()

std::vector< std::pair<int, int> >
MeshUtil::find_pairs( MyMesh mesh1, MyMesh mesh2)
{
  std::vector< std::pair<int, int> > pairs;

  MyMesh::VertexIter v_it1, v_end1( mesh1.vertices_end());
  for ( v_it1 = mesh1.vertices_begin(); v_it1 != v_end1; ++v_it1)
  {

    MyMesh::VertexIter v_it2, v_end2( mesh2.vertices_end());
    for ( v_it2 = mesh2.vertices_begin(); v_it2 != v_end2; ++v_it2)
    {
      if ( compare_vertices( *v_it1, mesh1, *v_it2, mesh2))
      {
        std::cout << "match" << std::endl;
        pairs.push_back( std::make_pair( ( *v_it1).idx(), ( *v_it2).idx()));
      }
    }

  }

  return pairs;
}

// ---------- fill_in_faces()

void
MeshUtil::fill_in_faces()
{
  MyMesh::HalfedgeIter he_it, he_end(mesh.halfedges_end());
  for ( he_it = mesh.halfedges_begin(); he_it != he_end; ++he_it)
  {
    if ( mesh.is_boundary( *he_it))
    {
      MyMesh::VertexHandle vh_begin = mesh.from_vertex_handle( *he_it);
      MyMesh::HalfedgeHandle heh = *he_it;

      bool valid_face = true;

      std::vector<MyMesh::VertexHandle> v_handles;
      size_t edge_count = 0;
      do
      {
        if ( edge_count > 40)
        {
          valid_face = false;
          break;
        }
        v_handles.push_back( mesh.from_vertex_handle( heh));
        heh = mesh.next_halfedge_handle( heh);
        ++edge_count;
      }
      while ( mesh.from_vertex_handle( heh) != vh_begin);

      if ( valid_face) mesh.add_face( v_handles);
    }
  }
}

// ---------- paste_horizontal()

void
MeshUtil::paste_horizontal( int width, double patch_w, std::string patchfile, std::string stripfile)
{
  MyMesh patch;
  OpenMesh::IO::read_mesh( patch, patchfile);

  MyMesh::VertexIter v_it1, v_end1( patch.vertices_end());
  std::vector<MyMesh::Point> vertices;
  for ( v_it1 = patch.vertices_begin(); v_it1 != v_end1; ++v_it1)
  {
    vertices.push_back( patch.point( *v_it1));
  }

  std::ostringstream oss;

  std::vector<std::string> faces = read_faces( patchfile);

  // horizontal direction

  for ( int count_w = 1; count_w < width; count_w++)
  {
    std::vector<MyMesh::VertexHandle> border_L, border_R;
    MyMesh::VertexIter v_it, v_end( mesh.vertices_end());
    for ( v_it = mesh.vertices_begin(); v_it != v_end; ++v_it)
    {
      if ( mesh.point( *v_it)[0] == 0)
      {
        border_L.push_back( *v_it);
        std::cout << "L: " << mesh.point( *v_it) << std::endl;
      }
      if ( mesh.point( *v_it)[0] == patch_w * count_w)
      {
        border_R.push_back( *v_it);
        std::cout << "R: " << mesh.point( *v_it) << std::endl;
      }
    }

    // Write a new patch out to OFF
    std::ofstream out( "temp2.off");
    if ( out.is_open())
    {
      out << "OFF" << std::endl;
      out << vertices.size() << ' ' << faces.size() << " 0" << std::endl;

      for ( size_t i = 0; i < vertices.size(); i++)
      {
        if ( count_w % 2 == 1)
        {
          out << vertices[i][0] + ( patch_w * count_w)
            << ' ' << ( vertices[i][1] + 2) << " 0" << std::endl;
        }
        else
        {
          out << vertices[i][0] + ( patch_w * count_w)
            << ' ' << vertices[i][1] << " 0" << std::endl;
        }
      }

      for ( size_t i = 0; i < faces.size(); i++)
      {
        out << faces[i] << std::endl;
      }
      out.close();
    }

    MyMesh mesh2;
    OpenMesh::IO::read_mesh( mesh2, "temp2.off");

    std::vector< std::pair< int, int> > paired_vert;
    MyMesh::VertexIter v_it2, v_end2(mesh2.vertices_end());
    for ( v_it2 = mesh2.vertices_begin(); v_it2 != v_end2; ++v_it2)
    {
      for ( v_it = mesh.vertices_begin(); v_it != v_end; ++v_it)
      {
        if ( mesh2.point( *v_it2)[0] == mesh.point( *v_it)[0]
          && mesh2.point( *v_it2)[1] == mesh.point( *v_it)[1])
        {
          oss << *v_it;
          int v = std::stoi( oss.str());
          oss.str("");
          oss << *v_it2;
          int v2 = std::stoi( oss.str());
          oss.str("");
          paired_vert.push_back( std::make_pair( v, v2));
        }
      }
    }

    OpenMesh::IO::write_mesh( mesh, "temp.off");
    merge_OFF( "temp.off", "temp2.off", paired_vert, stripfile);

    std::cout << "reading in " << stripfile << std::endl;

    OpenMesh::IO::read_mesh( mesh, stripfile);

    std::cout << "end of loop" << std::endl;
  }
}

void
MeshUtil::stitch_patches( int width, int height, char* patchfile)
{
  double patch_w, patch_h = 0;
  MyMesh::VertexIter v_it_p, v_end_p( mesh.vertices_end());
  for ( v_it_p = mesh.vertices_begin(); v_it_p != v_end_p; ++v_it_p)
  {
    if ( mesh.point( *v_it_p)[0] > patch_w)
      patch_w = mesh.point( *v_it_p)[0];
    if ( mesh.point( *v_it_p)[1] > patch_h)
      patch_h = mesh.point( *v_it_p)[1];
  }

  patch_h -=3.1;
  patch_w -=0.5;

  paste_horizontal( width, patch_w, patchfile, "strip.off");

  OpenMesh::IO::read_mesh( mesh, "strip.off");

  // vertical direction

  MyMesh strip;
  OpenMesh::IO::read_mesh( strip, "strip.off");

  std::vector<MyMesh::Point> vertices;
  MyMesh::VertexIter v_it_s, v_end_s( strip.vertices_end());
  for ( v_it_s = strip.vertices_begin(); v_it_s != v_end_s; ++v_it_s)
  {
    vertices.push_back( strip.point( *v_it_s));
  }

  std::vector<std::string> faces = read_faces( "strip.off");

  for ( int count_h = 1; count_h < height; count_h++)
  {
    MyMesh::VertexIter v_it, v_end( mesh.vertices_end());

    // Write a new strip out to OFF
    std::ofstream out( "temp2.off");
    if ( out.is_open())
    {
      out << "OFF" << std::endl;
      out << vertices.size() << ' ' << faces.size() << " 0" << std::endl;

      for ( size_t i = 0; i < vertices.size(); i++)
      {
          out << vertices[i][0] + count_h << ' '
            << vertices[i][1] + ( patch_h * count_h) << " 0" << std::endl;
      }

      for ( size_t i = 0; i < faces.size(); i++)
      {
        out << faces[i] << std::endl;
      }
      out.close();
    }

    MyMesh mesh2;
    OpenMesh::IO::read_mesh( mesh2, "temp2.off");

    std::vector< std::pair< int, int> > paired_vert;
    std::ostringstream oss;
    MyMesh::VertexIter v_it2, v_end2(mesh2.vertices_end());
    for ( v_it2 = mesh2.vertices_begin(); v_it2 != v_end2; ++v_it2)
    {
      for ( v_it = mesh.vertices_begin(); v_it != v_end; ++v_it)
      {
        // if both degree 2
        MyMesh::VertexVertexIter vv_it, vv_it2;
        size_t deg, deg2;
        deg = 0;
        deg2 = 0;
        for (vv_it=mesh.vv_iter( *v_it ); vv_it.is_valid(); ++vv_it)
          deg++;
        for (vv_it2=mesh2.vv_iter( *v_it2 ); vv_it2.is_valid(); ++vv_it2)
          deg2++;
        if ( deg == 2 && deg2 == 2 && equalish( mesh2.point( *v_it2)[0], mesh.point( *v_it)[0])
          && equalish( mesh2.point( *v_it2)[1], mesh.point( *v_it)[1]))
        {
          oss << *v_it;
          int v = std::stoi( oss.str());
          oss.str("");
          oss << *v_it2;
          int v2 = std::stoi( oss.str());
          oss.str("");
          paired_vert.push_back( std::make_pair( v, v2));
        }
      }
    }

    OpenMesh::IO::write_mesh( mesh, "temp.off");
    merge_OFF( "temp.off", "temp2.off", paired_vert, "merge.off");

    std::cout << "reading in merge.off" << std::endl;

    OpenMesh::IO::read_mesh( mesh, "merge.off");

    std::cout << "end of loop" << std::endl;
  }

}

void
MeshUtil::add_mass_redux()
{
  std::vector<MyMesh::FaceHandle> del_handles;
  MyMesh::FaceIter f_it, f_end( mesh.faces_end());
  for ( f_it = mesh.faces_sbegin(); f_it != f_end; f_it++)
  {
      MyMesh::FaceVertexIter fv_it;
      size_t vcount = 0;
      for (fv_it=mesh.fv_iter( *f_it ); fv_it.is_valid(); ++fv_it)
      {
        vcount++;
      }
      if ( vcount > 3)
      {
        del_handles.push_back( *f_it);
      }
      else if ( vcount == 3)
      {
        MyMesh::FaceFaceIter ff_it;
        size_t adj_tri_count = 0;
        for (ff_it=mesh.ff_iter( *f_it ); ff_it.is_valid(); ++ff_it)
        {
          MyMesh::FaceVertexIter ffv_it;
          size_t ffvcount = 0;
          for (ffv_it=mesh.fv_iter( *ff_it ); ffv_it.is_valid(); ++ffv_it)
          {
            ffvcount++;
          }
          if ( ffvcount == 3)
          {
            adj_tri_count++;
          }
        }
        if ( adj_tri_count == 3)
        {
          del_handles.push_back( *f_it);
        }
      }
  }

  for ( size_t i = 0; i < del_handles.size(); ++i)
  {
    mesh.delete_face( del_handles[i], true);
  }

  mesh.garbage_collection();

}

void
MeshUtil::paste_different_horizontal( double patch_w, MyMesh patchL, MyMesh patchR, std::string stripfile)
{
  OpenMesh::IO::write_mesh( patchL, "patchL.off");
  OpenMesh::IO::write_mesh( patchR, "patchR.off");

  MyMesh::VertexIter v_it1, v_end1( patchR.vertices_end());
  std::vector<MyMesh::Point> vertices;
  for ( v_it1 = patchR.vertices_begin(); v_it1 != v_end1; ++v_it1)
  {
    vertices.push_back( patchR.point( *v_it1));
  }
  std::vector<std::string> faces = read_faces( "patchR.off");


  std::ostringstream oss;

  // -------- horizontal direction

  MyMesh::VertexIter v_it, v_end( patchL.vertices_end());

  // Write a new patch out to OFF
  std::ofstream out( "temp2.off");
  if ( out.is_open())
  {
    out << "OFF" << std::endl;
    out << vertices.size() << ' ' << faces.size() << " 0" << std::endl;

    for ( size_t i = 0; i < vertices.size(); i++)
    {
        out << vertices[i][0] + patch_w
          << ' ' << ( vertices[i][1] + 2) << " 0" << std::endl;
    }

    for ( size_t i = 0; i < faces.size(); i++)
    {
      out << faces[i] << std::endl;
    }
    out.close();
  }

  MyMesh mesh2;
  OpenMesh::IO::read_mesh( mesh2, "temp2.off");

  std::vector< std::pair< int, int> > paired_vert;
  MyMesh::VertexIter v_it2, v_end2(mesh2.vertices_end());
  for ( v_it2 = mesh2.vertices_begin(); v_it2 != v_end2; ++v_it2)
  {
    for ( v_it = patchL.vertices_begin(); v_it != v_end; ++v_it)
    {
      if ( mesh2.point( *v_it2)[0] == patchL.point( *v_it)[0]
        && mesh2.point( *v_it2)[1] == patchL.point( *v_it)[1])
      {
        oss << *v_it;
        int v = std::stoi( oss.str());
        oss.str("");
        oss << *v_it2;
        int v2 = std::stoi( oss.str());
        oss.str("");
        paired_vert.push_back( std::make_pair( v, v2));
      }
    }
  }

  OpenMesh::IO::write_mesh( patchL, "temp.off");
  merge_OFF( "temp.off", "temp2.off", paired_vert, stripfile);

}

void
MeshUtil::stitch_different( MyMesh patch1, MyMesh patch2, MyMesh patch3)
{
  double patch_w, patch_h = 0;
  MyMesh::VertexIter v_it_p, v_end_p( mesh.vertices_end());
  for ( v_it_p = mesh.vertices_begin(); v_it_p != v_end_p; ++v_it_p)
  {
    if ( mesh.point( *v_it_p)[0] > patch_w)
      patch_w = mesh.point( *v_it_p)[0];
    if ( mesh.point( *v_it_p)[1] > patch_h)
      patch_h = mesh.point( *v_it_p)[1];
  }

  patch_h -=1;
  patch_w -=0.5;

  paste_different_horizontal( patch_w, mesh, patch1, "bottom-strip.off");
  paste_different_horizontal( patch_w, patch2, patch3, "top-strip.off");

  OpenMesh::IO::read_mesh( mesh, "bottom-strip.off");


  // -------- vertical direction

  MyMesh top_strip;
  OpenMesh::IO::read_mesh( top_strip, "top-strip.off");

  std::vector<MyMesh::Point> vertices;
  MyMesh::VertexIter v_it_s, v_end_s( top_strip.vertices_end());
  for ( v_it_s = top_strip.vertices_begin(); v_it_s != v_end_s; ++v_it_s)
  {
    vertices.push_back( top_strip.point( *v_it_s));
  }

  std::vector<std::string> faces = read_faces( "top-strip.off");


  MyMesh::VertexIter v_it, v_end( mesh.vertices_end());

  // Write a new strip out to OFF
  std::ofstream out( "temp2.off");
  if ( out.is_open())
  {
    out << "OFF" << std::endl;
    out << vertices.size() << ' ' << faces.size() << " 0" << std::endl;

    for ( size_t i = 0; i < vertices.size(); i++)
    {
        out << vertices[i][0] + 1 << ' '
          << vertices[i][1] + patch_h << " 0" << std::endl;
    }

    for ( size_t i = 0; i < faces.size(); i++)
    {
      out << faces[i] << std::endl;
    }
    out.close();
  }

  OpenMesh::IO::read_mesh( top_strip, "temp2.off");

  std::vector< std::pair< int, int> > paired_vert;
  std::ostringstream oss;
  for ( v_it_s = top_strip.vertices_begin(); v_it_s != v_end_s; ++v_it_s)
  {
    for ( v_it = mesh.vertices_begin(); v_it != v_end; ++v_it)
    {
      // if both degree 2
      MyMesh::VertexVertexIter vv_it, vv_it_s;
      size_t deg, deg2;
      deg = 0;
      deg2 = 0;
      for (vv_it=mesh.vv_iter( *v_it ); vv_it.is_valid(); ++vv_it)
        deg++;
      for (vv_it_s=top_strip.vv_iter( *v_it_s ); vv_it_s.is_valid(); ++vv_it_s)
        deg2++;
      if ( deg == 2 && deg2 == 2 && equalish( top_strip.point( *v_it_s)[0], mesh.point( *v_it)[0])
        && equalish( top_strip.point( *v_it_s)[1], mesh.point( *v_it)[1]))
      {
        oss << *v_it;
        int v = std::stoi( oss.str());
        oss.str("");
        oss << *v_it_s;
        int v2 = std::stoi( oss.str());
        oss.str("");
        paired_vert.push_back( std::make_pair( v, v2));
      }
    }
  }

  OpenMesh::IO::write_mesh( mesh, "temp.off");
  merge_OFF( "temp.off", "temp2.off", paired_vert, "merge.off");

  std::cout << "reading in merge.off" << std::endl;

  OpenMesh::IO::read_mesh( mesh, "merge.off");

}

#endif
