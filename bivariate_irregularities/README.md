### README for bivariate irregularities

Outputs a triangulation with user inputted size and number of flips.

###### To run:

In build/src/OpenMesh/Apps/bivariate_irregularities:
```
./bivariate_irregularities outfile.off width height number_of_flips
```

Where `width`, `height` and `number_of_flips` are integers. `width` becomes the number of vertices in the x direction and `height` the number of vertices in the y direction.
`number_of_flips` determines how many edges are flipped in the triangulation.

For example, entering `5 8 7` will output a triangulation that is 5 vertices wide and 8 vertices high, with 7 flipped edges.
