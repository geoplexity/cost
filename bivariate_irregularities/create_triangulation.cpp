/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
// create_triangulation.cpp

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/OFF_reader.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>

#include <CGAL/enum.h>

#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <utility>
#include <stdexcept>
#include <cstdlib>
#include <ctime>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>         Triangulation;
typedef Triangulation::Vertex_circulator Vertex_circulator;
typedef Triangulation::Point             Point;
typedef Triangulation::Face_handle       Face_handle;

typedef CGAL::Polyhedron_3<K> Polyhedron;

void generate_points( int w, int h)
{
  std::ofstream pout("points.cin");
  for ( int i = 0; i < h; ++i)
  {
    for ( int j = 0; j < w; ++j)
    {
      if ( i % 2 == 0)
      {
        pout << (2 * j) << ' ' << (1.73205 * i) << '\n';
      }
      else
      {
        pout << (2 * j) + 1 << ' ' << (1.73205 * i) << '\n';
      }
    }
  }
  pout.close();
}

bool read_points( Triangulation& t, const char* infile)
{
  std::ifstream in( infile);
  if ( in.is_open())
  {
    std::istream_iterator<Point> begin( in);
    std::istream_iterator<Point> end;
    t.insert( begin, end);
    in.close();
    return true;
  }
  else
  {
    return false;
  }
}

bool print_OFF( Triangulation t, const char* outfile)
{
  std::vector<Point> vertices;
  std::vector< std::vector<size_t> > faces;

  Triangulation::Finite_vertices_iterator vit;
  for (vit = t.finite_vertices_begin(); vit != t.finite_vertices_end(); ++vit)
  {
    vertices.push_back( vit->point());
  }

  Triangulation::Finite_faces_iterator fit;
  for (fit = t.finite_faces_begin(); fit != t.finite_faces_end(); ++fit)
  {
    Triangulation::Face_handle fh = fit;
    std::vector<size_t> face;

    for ( size_t i = 0; i < vertices.size(); i++)
    {
      if ( fh->vertex(0)->point() == vertices[i]
        || fh->vertex(1)->point() == vertices[i]
        || fh->vertex(2)->point() == vertices[i])
      {
        face.push_back( i);
      }
    }

    faces.push_back( face);
  }

  std::ofstream out( outfile);
  if ( out.is_open())
  {
    out << "OFF" << std::endl;
    out << vertices.size() << ' ' << faces.size() << " 0" << std::endl;

    for ( size_t i = 0; i < vertices.size(); i++)
    {
      out << vertices[i].x() << ' ' << vertices[i].y() << " 0" << std::endl;
    }

    for ( size_t i = 0; i < faces.size(); i++)
    {
      out << faces[i].size() << "  ";
      for ( size_t j = 0; j < faces[i].size(); j++)
      {
        out << faces[i][j] << ' ';
      }
      out << std::endl;
    }
    out.close();
    return true;
  }
  else
  {
    return false;
  }
}

bool is_flipable( Face_handle f, int i, Triangulation t)
  // determines if edge (f,i) can be flipped
{
  if ( f == NULL) return false;
  Face_handle ni = f->neighbor(i);
  if ( t.is_infinite(f) || t.is_infinite(ni)) return false;
  // if (f->is_constrained(i)) return false;
  return ( t.orientation(f->vertex(i)->point(),
                f->vertex(t.cw(i))->point(),
                t.mirror_vertex(f,i)->point()) == CGAL::RIGHT_TURN &&
    t.orientation(f->vertex(i)->point(),
                f->vertex(t.ccw(i))->point(),
                t.mirror_vertex(f,i)->point()) == CGAL::LEFT_TURN);
}

int main( int argc, char **argv)
{
  if (argc != 5)
  {
    std::cerr << "Usage:  " << argv[0] << " #iterations outfile width height number_of_flips\n";
    return 1;
  }

  int w, h;

  std::istringstream ss( argv[2]);
  if ( !( ss >> w)) throw std::runtime_error( "Invalid width");
  ss.str("");

  ss.str( argv[3]);
  if ( !( ss >> h)) throw std::runtime_error( "Invalid height");
  ss.str("");

  generate_points( w, h);

  Triangulation t;

  if ( ! read_points( t, "points.cin"))
  {
    std::cerr << "Error: Cannot read points from " << "points.cin" << std::endl;
    return 1;
  }

  int number_of_flips;
  ss.str( argv[4]);
  if ( !( ss >> number_of_flips)) throw std::runtime_error( "Invalid number of flips");

  std::srand(std::time(0));

  for ( int i = 0; i < number_of_flips; ++i)
  {
    for ( int j = 0; j < 3; ++j)
    {
      double x_coord = 4 + ( (double)std::rand() / RAND_MAX) * ( ( ( ( w - 1) * 2) - 8) + 1);
      double y_coord = 4 + ( (double)std::rand() / RAND_MAX) * ( ( ( h - 1) * 2) - 8);

      Point p( x_coord, y_coord);
      Triangulation::Face_handle fh = t.locate(p);
      int edge_index = std::rand() % 3;
      if ( is_flipable( fh, edge_index, t))
      {
        t.flip( fh, edge_index);
        break;
      }
      else if ( is_flipable( fh, (edge_index + 1) % 3, t))
      {
        t.flip( fh, (edge_index + 1) % 3);
        break;
      }
      else if ( is_flipable( fh, (edge_index + 2) % 3, t))
      {
        t.flip( fh, (edge_index + 2) % 3);
        break;
      }
      else
      {
      }
    }
  }

  if ( ! print_OFF( t, argv[1]))
  {
    std::cerr << "Error: cannot write triangulation to " << argv[1] << std::endl;
    return 1;
  }

  // ===========================================================================
  // 16 Jan 2018 I added this part here because I was getting an error when I
  // tried to read in the OFF of the triangulation into OpenMesh. OpenMesh is
  // more sensitive to the order that vertices are added, so I believe the code
  // below ensures that the vertices are in the correct order in the OFF.

  std::ifstream input( argv[1]);
  if (!input)
  {
   std::cerr << "Cannot open file " << std::endl;
   return 1;
  }
  std::vector<K::Point_3> points;
  std::vector< std::vector<std::size_t> > polygons;
  if ( !CGAL::read_OFF( input, points, polygons))
  {
   std::cerr << "Error parsing the OFF file " << std::endl;
   return 1;
  }
  CGAL::Polygon_mesh_processing::orient_polygon_soup( points, polygons);
  Polyhedron mesh;
  CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh( points, polygons, mesh);
  if (CGAL::is_closed(mesh) && (!CGAL::Polygon_mesh_processing::is_outward_oriented( mesh)))
   CGAL::Polygon_mesh_processing::reverse_face_orientations(mesh);
  std::ofstream out( argv[1]);
  out << mesh;
  out.close();

  // ===========================================================================

  return 0;
}
