/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <bivariate_refinement.h>
// -------------------- OpenMesh
// #include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// reads in a mesh from argv[1] and outputs mesh to argv[2]

int CGAL_fix( char* filename)
{
  std::ifstream input( filename);
  if (!input)
  {
   std::cerr << "Cannot open file " << std::endl;
   return 1;
  }
  std::vector<K::Point_3> points;
  std::vector< std::vector<std::size_t> > polygons;
  if ( !CGAL::read_OFF( input, points, polygons))
  {
   std::cerr << "Error parsing the OFF file " << std::endl;
   return 1;
  }
  CGAL::Polygon_mesh_processing::orient_polygon_soup( points, polygons);
  Polyhedron mesh;
  CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh( points, polygons, mesh);
  if (CGAL::is_closed(mesh) && (!CGAL::Polygon_mesh_processing::is_outward_oriented( mesh)))
   CGAL::Polygon_mesh_processing::reverse_face_orientations(mesh);
  std::ofstream out( filename);
  out << mesh;
  out.close();
  return 0;
}




int main( int argc, char **argv)
{
  MeshUtil mesh_util;
  // check command line options
  if (argc != 4)
  {
    std::cerr << "Usage:  " << argv[0] << " #iterations infile outfile refinement\n";
    return 1;
  }
  if ( ! OpenMesh::IO::read_mesh( mesh_util.mesh, argv[1]) )
  {
    std::cerr << "Error: Cannot read mesh from " << argv[1] << std::endl;
    return 1;
  }

  mesh_util.fill_in_faces();


  //----------- read in binary string ------------------------------------------
  std::string refinement_str( argv[3]);
  //----------------------------------------------------------------------------

  for ( size_t index = 0; index < refinement_str.length(); index++)
  {
    std::vector<MyMesh::FaceHandle> faces_to_split;

    MyMesh::FaceIter f_it, f_end( mesh_util.mesh.faces_end());
    for ( f_it = mesh_util.mesh.faces_begin(); f_it != f_end; ++f_it)
    {
      MyMesh::HalfedgeHandle heh = mesh_util.mesh.halfedge_handle( *f_it);
      MyMesh::HalfedgeHandle heh_end = heh;

      size_t counter = 0;
      do
      {
        counter++;
        heh = mesh_util.mesh.next_halfedge_handle( heh);
      } while ( heh != heh_end);

      if ( refinement_str[index] == '1')
      {
        if ( counter > 3)
        {
          faces_to_split.push_back( *f_it);
        }
      }
      else if ( refinement_str[index] == '0')
      {
        if ( counter == 3)
        {
          bool valid_triangle = true;

          for ( size_t k = 0; k < faces_to_split.size(); ++k)
          {
            if ( mesh_util.mesh.face_handle( mesh_util.mesh.opposite_halfedge_handle( heh)) == faces_to_split[k])
            {
              heh = mesh_util.mesh.next_halfedge_handle( heh);
              for ( size_t l = 0; l < faces_to_split.size(); ++l)
              {
                if ( mesh_util.mesh.face_handle( mesh_util.mesh.opposite_halfedge_handle( heh)) == faces_to_split[l])
                {
                  valid_triangle = false;
                  break;
                }
              }
              heh = mesh_util.mesh.next_halfedge_handle( heh);
              for ( size_t l = 0; l < faces_to_split.size(); ++l)
              {
                if ( mesh_util.mesh.face_handle( mesh_util.mesh.opposite_halfedge_handle( heh)) == faces_to_split[l])
                {
                  valid_triangle = false;
                  break;
                }
              }
            }
          }

          if ( valid_triangle)
          {
            faces_to_split.push_back( *f_it);
          }

        }
      }
      else
      {
        throw std::runtime_error( "invalid refinement input");
      }
    }

    for ( size_t i = 0; i < faces_to_split.size(); i++)
    {
      mesh_util.split_and_connect( faces_to_split[i]);
    }
  }
  // ---------------------------------------------------------------------------


  if ( ! OpenMesh::IO::write_mesh( mesh_util.mesh, argv[2]) )
  {
    std::cerr << "Error: cannot write mesh to " << argv[2] << std::endl;
    return 1;
  }

  return 0;
}
