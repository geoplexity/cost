### README for bivariate refinement

Takes as input a CoST and refinement and outputs the CoST with refinement.

###### To run:

In build/src/OpenMesh/Apps/bivariate_refinement:
```
./bivariate_refinement infile.off outfile.off refinement
```

where `infile.off` is an OFF file that describe a CoST,
and `refinement` is a binary string.

A refinement value of `01` will first split all triangular faces, and then split
all non-triangular faces. A refinement value of `110` will split all
non-triangular faces, then split all non-triangular faces again, then split all
triangular faces.
