/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
#include <CoST_from_triangulation.h>

int main( int argc, char **argv)
{
  MeshUtil mesh_util;

  // check command line options
  if (argc != 3)
  {
    std::cerr << "Usage:  " << argv[0] << " #iterations infile outfile\n";
    return 1;
  }
  if ( ! OpenMesh::IO::read_mesh( mesh_util.mesh, argv[1]) )
  {
    std::cerr << "Error: Cannot read mesh from " << argv[1] << std::endl;
    return 1;
  }

  mesh_util.tri_to_trihex();

  if ( ! OpenMesh::IO::write_mesh( mesh_util.mesh, argv[2]) )
  {
    std::cerr << "Error: cannot write mesh to " << argv[2] << std::endl;
    return 1;
  }

  return 0;
}
