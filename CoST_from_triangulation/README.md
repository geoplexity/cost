### README for CoST from triangulation

###### To run:

In build/src/OpenMesh/Apps/CoST_from_triangulation:
```
./CoST_from_triangulation infile.off outfile.off
```
where `infile.off` is a triangulation.
