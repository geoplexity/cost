### README for trivariate CoST with irregularities

Takes as input bivariate CoST.
Layers the CoST into a cristobalite with _m_ layers, then adds _n_ irregularities.

###### To run:

In build/src/OpenMesh/Apps/bivariate_refinement:
```
./bivariate_refinement infile.off outfile.off number_of_layers number_of_irregularities
```

where `infile.off` is an OFF file that describe a CoST and `number_of_layers` is
the number of stacked layers of the CoST to add to create the cristobalite structure.
For example, `infile.off outfile.off 5 13` will output a cristobalite to `outfile.off`
that is 5 layers high, and has 13 flipped edges.
