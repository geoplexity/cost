/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <trivariate_CoST_with_irregularities.h>
// -------------------- OpenMesh
// #include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// reads in a mesh from argv[1] and outputs mesh to argv[2]

int CGAL_fix( char* filename)
{
  std::ifstream input( filename);
  if (!input)
  {
   std::cerr << "Cannot open file " << std::endl;
   return 1;
  }
  std::vector<K::Point_3> points;
  std::vector< std::vector<std::size_t> > polygons;
  if ( !CGAL::read_OFF( input, points, polygons))
  {
   std::cerr << "Error parsing the OFF file " << std::endl;
   return 1;
  }
  CGAL::Polygon_mesh_processing::orient_polygon_soup( points, polygons);
  Polyhedron mesh;
  CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh( points, polygons, mesh);
  if (CGAL::is_closed(mesh) && (!CGAL::Polygon_mesh_processing::is_outward_oriented( mesh)))
   CGAL::Polygon_mesh_processing::reverse_face_orientations(mesh);
  std::ofstream out( filename);
  out << mesh;
  out.close();
  return 0;
}




int main( int argc, char **argv)
{
  MeshUtil mesh_util;
  // check command line options
  if (argc != 5)
  {
    std::cerr << "Usage:  " << argv[0] << " #iterations infile outfile number_of_layers number_of_irregularities\n";
    return 1;
  }
  if ( ! OpenMesh::IO::read_mesh( mesh_util.mesh, argv[1]) )
  {
    std::cerr << "Error: Cannot read mesh from " << argv[1] << std::endl;
    return 1;
  }

  mesh_util.fill_in_faces();

  size_t l;
    std::istringstream ss( argv[3]);
  if ( !( ss >> l)) throw std::runtime_error( "Invalid number of layers");
  ss.str("");

  mesh_util.add_layers( l);
  mesh_util.connect_layers( l);

  size_t n_irreg;
  ss.str( argv[4]);
  if ( !( ss >> n_irreg)) throw std::runtime_error( "Invalid number of irregularities");

  MyMesh::FaceHandle fh1;
  MyMesh::FaceHandle fh2;
  std::srand(std::time(0));
  for ( int i = 0; i < n_irreg; i++)
  {
    size_t num;
    num = std::rand() % mesh_util.mesh.n_faces();

    size_t count = 0;
    for ( MyMesh::FaceIter f_it = mesh_util.mesh.faces_sbegin(); f_it != mesh_util.mesh.faces_end(); ++f_it)
    {
      if ( mesh_util.is_triangle( *f_it) && mesh_util.check_layer( *f_it, (1.73205*2)))
      {
        if ( count > num)
        {
          fh1 = *f_it;
          break;
        }
        else count++;
      }
    }
    for ( MyMesh::FaceVertexIter fv_it = mesh_util.mesh.fv_iter( fh1); fv_it.is_valid(); ++fv_it)
    {
      bool match = false;
      for ( MyMesh::VertexFaceIter vf_it = mesh_util.mesh.vf_iter( *fv_it); vf_it.is_valid(); ++vf_it)
      {
        if ( *vf_it != fh1 && mesh_util.is_triangle( *vf_it) && mesh_util.check_layer( *vf_it, (1.73205*2)))
        {
          fh2 = *vf_it;
          match = true;
          break;
        }
      }
      if ( match) break;
    }
    mesh_util.irregulate( fh1, fh2);
  }


  mesh_util.add_mass_redux();

  if ( ! OpenMesh::IO::write_mesh( mesh_util.mesh, argv[2]) )
  {
    std::cerr << "Error: cannot write mesh to " << argv[2] << std::endl;
    return 1;
  }

  return 0;
}
