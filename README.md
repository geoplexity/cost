# CoST

+Corner Sharing Triangle and Tetrahedra (bivariate and trivariate CoST) discrete microstructure representations:

+A suite methods for

+--bivariate and trivariate CoST generation from underlying regular triangulations and square grids -- within adomain of desired size and translate

+--refinement of various types (tailored removal of mass)

+--adding irregularities

+--converting a desired non-edge into an edge through addition of irregularities

+--balancing the edges around points so that CoST can represent a tensegrity with all positive or all negative stresses (such as a packing)

+--gluing two CoSTs

+--stabilizing boundary to rigidify

+

+etc.


## Build OpenMesh

Find information on building OpenMesh here: http://www.openmesh.org/media/Documentations/OpenMesh-Doc-Latest/a03933.html

## Install CGAL

Find information on installing CGAL here: https://doc.cgal.org/latest/Manual/installation.html

## Build Projects

To build, make sure that you have the requirements to build OpenMesh (https://www.openmesh.org/media/Documentations/OpenMesh-6.2-Documentation/a00030.html).

In addition, you need to have Boost, CGAL, and gmp installed.

Then, create a "Build" folder parallel to "Source."
Then run:

`cmake ..`      (Generates the appropriate Makefiles)

`make`              (Builds the project)

See http://www.openmesh.org/media/Documentations/OpenMesh-Doc-Latest/a00017.html for more info on compiling OpenMesh.


To add the projects to OpenMesh, from wherever you built OpenMesh, drop each project directory (CoST from triangulation, bivariate refinement after irregularities, bivariate irregularities, etc) into src/OpenMesh/Apps. Then change the CMakeLists.txt that is already in src/OpenMesh/Apps to **the CMakeLists.txt that is in the top directory of this repository.** Go back to the build directory and rebuild.


## Adding a project

Create a new folder in src/OpenMesh/Apps and name it the name of the new project. Drop whatever code into the new folder, and then add a CMakeLists.txt and on the line `set (targetName putProjectNameHere)` change the project name to the name of the project. Make sure `CGAL` and `gmp` are included under `target_link_libraries` so it looks like this:
```
target_link_libraries (${targetName}
  OpenMeshCore
  OpenMeshTools
  CGAL
  gmp
```
Then add this line to the CMakeLists.txt that’s in src/OpenMesh/Apps:
```
add_subdirectory (projectName)
```
and then rebuild. Go to build/src/OpenMesh/Apps/projectName and run from there.

## License

CoST is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. The GNU GPL license can be found at http://www.gnu.org/licenses/.

